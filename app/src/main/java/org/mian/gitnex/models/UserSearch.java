package org.mian.gitnex.models;

import java.util.ArrayList;

/**
 * Author M M Arif
 */

public class UserSearch {

    private ArrayList<UserInfo> data;
    private Boolean ok;

    public ArrayList<UserInfo> getData() {
        return data;
    }

    public Boolean getOk() {
        return ok;
    }

}